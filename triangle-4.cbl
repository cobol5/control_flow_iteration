       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-4.
       AUTHOR. JIRAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE    PIC X(80)   VALUE SPACES .
       01  STAR-NUM    PIC 9(3)    VALUE ZEROES .
           88 VALID-STAR-NUM VALUE 1 THRU 80.
       01  INDEX-NUM1   PIC 9(3)    VALUE ZEROES .
       01  INDEX-NUM2   PIC 9(3)    VALUE ZEROES .

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-INPUT-STAR-NUM THRU 001-EXIT 
           PERFORM VARYING INDEX-NUM1 FROM STAR-NUM  BY -1
                    UNTIL INDEX-NUM1 = 0
                    COMPUTE INDEX-NUM2 = STAR-NUM - INDEX-NUM1 + 1
                    MOVE ALL SPACES TO SCR-LINE
                    MOVE ALL "*" TO SCR-LINE(INDEX-NUM2:INDEX-NUM1)
                    DISPLAY SCR-LINE
           END-PERFORM
           GOBACK
       .

       001-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM
              DISPLAY "Please input star number: " WITH NO ADVANCING
              ACCEPT STAR-NUM
                 IF NOT VALID-STAR-NUM
                 DISPLAY "Please input star number in positive number"
           END-PERFORM

       .

       001-EXIT.
           EXIT
       .
